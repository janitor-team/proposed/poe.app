/*
    Util.m - Class to manage the 'standard' vorbis comment 
    fields for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "Util.h"

static Util *singleInstance = nil;

@implementation Util 

- (void) dealloc
{
  RELEASE(_tags);
  RELEASE(_tagsTitle);
  RELEASE(_tagsDescription);
  RELEASE(_genres);
 
  [[NSNotificationCenter defaultCenter] removeObserver: self];

  [super dealloc];
}

- (id) init
{
  int i;
  NSDictionary *standardCommentInfo;
  NSMutableArray *tempArray;

  self = [super init];

  NSString *path = [[NSBundle mainBundle] pathForResource: @"CommentInfo" ofType: @"plist"];
  standardCommentInfo = [NSDictionary dictionaryWithContentsOfFile: path];

  tempArray = [NSMutableArray arrayWithArray:
    [standardCommentInfo objectForKey: @"tagsTitle"]];

  for(i=0;i<[tempArray count];i++)
    {
      [tempArray replaceObjectAtIndex: i withObject:
        NSLocalizedStringFromTable([tempArray objectAtIndex: i], @"CommentInfo.strings",nil)];
    }

  _tagsTitle = [[NSArray alloc] initWithArray: tempArray];

  tempArray = [NSMutableArray arrayWithArray:
    [standardCommentInfo objectForKey: @"tagsDescription"]];

  for(i=0;i<[tempArray count];i++)
    {
      [tempArray replaceObjectAtIndex: i withObject:
        NSLocalizedStringFromTable([tempArray objectAtIndex: i], @"CommentInfo.strings",nil)];
    }

  _tagsDescription = [[NSArray alloc] initWithArray: tempArray];

  _tags = [[NSArray alloc] initWithArray:
    [standardCommentInfo objectForKey: @"tagsWritten"]];

  _genres = [[NSMutableArray alloc] initWithArray:
    [standardCommentInfo objectForKey: @"genres"]];

  [self syncGenres: nil];

  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(syncGenres:)
                                               name: @"GenresDidChange"
                                             object: nil];

  return self;
}

+ (id) singleInstance
{
  if( !singleInstance )
    {
      singleInstance = [[Util alloc] init];
    }

  return singleInstance;
}

- (void) syncGenres: (NSNotification *)not
{
  NSArray *gAdded;
  NSArray *gRemoved;
  int i;

  gAdded = [[NSUserDefaults standardUserDefaults] arrayForKey: @"genresAdded"];
  gRemoved = [[NSUserDefaults standardUserDefaults] arrayForKey: @"genresRemoved"];

  for(i=0;i<[gAdded count];i++)
    {
      if(![_genres containsObject: [gAdded objectAtIndex: i]])
        {
          [_genres addObject: [gAdded objectAtIndex: i]];
        }
    }

  for(i=0;i<[gRemoved count];i++)
    {
      if([_genres containsObject: [gRemoved objectAtIndex: i]])
        {
          int index = [_genres indexOfObject: [gRemoved objectAtIndex: i]];
          [_genres removeObjectAtIndex: index];
        }
    }
}

- (NSArray *)tags
{
  return _tags;
}

- (NSArray *)tagsTitle
{
  return _tagsTitle;
}

- (NSArray *)tagsDescription;
{
  return _tagsDescription;
}

- (NSArray *)genres
{
  return _genres;
}

- (NSString *) tagAtIndex: (int)index
{
  return [_tags objectAtIndex: index];
}

- (NSString *) titleAtIndex: (int)index
{
  return [_tagsTitle objectAtIndex: index];
}

- (NSString *) descriptionAtIndex: (int)index
{
  return [_tagsDescription objectAtIndex: index];
}

- (int) indexOfTag: (NSString *) tag
{
  int i;

  for(i=0;i<[_tags count];i++)
    {
      if([[_tags objectAtIndex: i] isEqualToString: tag])
        {
          return i;
        }
    }
  return -1;
}

- (int) indexOfTitle: (NSString *) title
{
  int i;

  for(i=0;i<[_tagsTitle count];i++)
    {
      if([[_tagsTitle objectAtIndex: i] isEqualToString: title])
        {
          return i;
        }
    }
  return -1;
}

- (NSArray *) tagTemplate
{
  NSMutableArray *template;
  NSArray *def;
  NSMutableDictionary *temp;
  int i;

  template = [NSMutableArray new];
  def = [[NSUserDefaults standardUserDefaults] arrayForKey: @"defaultTags"];

  for(i=0;i<[_tags count];i++)
    {
      temp = [NSMutableDictionary new];
      [temp setObject: [_tagsTitle objectAtIndex: i] forKey: @"title"];
      [temp setObject: [_tags objectAtIndex: i] forKey: @"tag"];
  
      if([def containsObject: [_tags objectAtIndex: i]])
        {
          [temp setObject: @"YES"  forKey: @"default"];
        }
      else
        {
          [temp setObject: @"NO" forKey: @"default"];
        }

      [template addObject: temp];
    }

  return [NSArray arrayWithArray: template];
}

- (NSArray*) formatComments: (NSArray *)arrayIn
{
  NSMutableDictionary *temp;
  NSMutableArray *arrayOut;
  int scount;
  int i,k,j;

  arrayOut = [NSMutableArray arrayWithArray:
    [self tagTemplate]];

  scount = [arrayOut count]-1;

  for(i=0;i<[arrayIn count];i++)
    {
      for(k=0;k<[arrayOut count];k++)
        {
          if([[[arrayOut objectAtIndex: k] objectForKey: @"tag"]
            isEqualToString: [[arrayIn objectAtIndex: i] objectForKey: @"tag"]] &&
            [[arrayOut objectAtIndex: k] objectForKey: @"value"] == nil)
            {
              if(![[arrayIn objectAtIndex: i] objectForKey: @"value"])
                {
                  [[arrayOut objectAtIndex: k] setObject: @""
                    forKey: @"value"];
                }
              else
                {
                  [[arrayOut objectAtIndex: k] setObject:
                    [[arrayIn objectAtIndex: i] objectForKey: @"value"]
                    forKey: @"value"];
                }

              break;
            }
          else if([[[arrayOut objectAtIndex: k] objectForKey: @"tag"]
            isEqualToString: [[arrayIn objectAtIndex: i] objectForKey: @"tag"]] &&
            [[arrayOut objectAtIndex: k] objectForKey: @"value"] != nil)
            {
              for(j=k;j<[arrayOut count];j++)
                {
                  if(![[[arrayOut objectAtIndex: j] objectForKey: @"tag"] isEqualToString:
                  [[arrayIn objectAtIndex: i] objectForKey: @"tag"]])
                    {
                      break;
                    }
                }

              if(![[arrayIn objectAtIndex: i] objectForKey: @"value"])
                {
                  temp = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    [[arrayOut objectAtIndex: k] objectForKey: @"title"], @"title",
                    [[arrayOut objectAtIndex: k] objectForKey: @"tag"], @"tag",
                    nil];
                }
              else
                {
                  temp = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                    [[arrayIn objectAtIndex: i] objectForKey: @"value"], @"value",
                    [[arrayOut objectAtIndex: k] objectForKey: @"title"], @"title",
                    [[arrayOut objectAtIndex: k] objectForKey: @"tag"], @"tag",
                    nil];
                }

              [arrayOut insertObject: temp atIndex: j];

              break;
            }
          else if(k == scount)
            {
              temp = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                [[arrayIn objectAtIndex: i] objectForKey: @"value"], @"value",
                [[arrayIn objectAtIndex: i] objectForKey: @"tag"], @"title",
                [[arrayIn objectAtIndex: i] objectForKey: @"tag"], @"tag",
                @"NO",@"default",
                nil];

              [arrayOut addObject: temp];

              break;
            }
        }
    }

  for(i=0;i<[arrayOut count];i++)
    {
      if([[arrayOut objectAtIndex: i] objectForKey: @"value"] == nil &&
        [[[arrayOut objectAtIndex: i] objectForKey: @"default"] isEqualToString: @"NO"])
        {
          [arrayOut removeObjectAtIndex: i];
          i--;
        }
    }

  return [NSArray arrayWithArray: arrayOut];
}

@end

