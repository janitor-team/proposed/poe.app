#!/usr/bin/make -f
#export DH_VERBOSE=1

include /usr/share/quilt/quilt.make
include /usr/share/GNUstep/debian/config.mk

export GNUSTEP_MAKEFILES := $(GS_MAKE_DIR)
d_app := $(CURDIR)/debian/poe.app

LDFLAGS := -Wl,-z,defs -Wl,--as-needed

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
optim := debug=yes
endif

build: patch build-stamp

build-stamp:
	dh_testdir
	$(MAKE) messages=yes $(optim) LDFLAGS="$(LDFLAGS)"
	convert Resources/GNUstep/Poe.tiff -resize 32x32 Poe.xpm
	touch $@

clean: clean-patched unpatch

clean-patched:
	dh_testdir
	dh_testroot
	$(MAKE) distclean
	dh_clean build-stamp Poe.xpm

install: build
	dh_testdir
	dh_testroot
	dh_prep
	$(MAKE) install GNUSTEP_INSTALLATION_DOMAIN=SYSTEM DESTDIR=$(d_app)
	dh_install debian/*.desktop usr/share/applications/
	dh_install Poe.xpm usr/share/pixmaps/
	rm $(d_app)$(GNUSTEP_SYSTEM_APPS)/*.app/Resources/*.desktop

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs 
	dh_installdocs
	dh_installmenu
	dh_installman debian/Poe.1
	dh_strip
ifeq ($(GS_USE_FHS),yes)
	gsdh_gnustep
	dh_installdirs usr/share/GNUstep
	mv $(d_app)$(GNUSTEP_SYSTEM_APPS)/Poe.app/Resources \
	  $(d_app)/usr/share/GNUstep/Poe.app
	dh_link usr/share/GNUstep/Poe.app \
	  $(GNUSTEP_SYSTEM_APPS)/Poe.app/Resources
endif
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
