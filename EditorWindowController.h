/*
    EditorWindowController.h - Editor window controller header for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#ifndef _EDITORWINDOWCONTROLLER_H_
#define _EDITORWINDOWCONTROLLER_H_

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "OGGEditor.h"
#include "AddPanelController.h"
#include "Util.h"

@interface EditorWindowController : NSWindowController
{
  id commentView;

  id length;
  id channels;
  id nominalOfBitrate;
  id upperOfBitrate;
  id lowerOfBitrate;
  id rate;

  OGGEditor *_oggFile;

  // for localization

  id lengthT;
  id channelsT;
  id sampleT;
  id bitrateT;
  id hbitrateT;
  id lbitrateT;
}

- (void) dealloc;
- (id) initWithEditor: (OGGEditor *) anOggFile;

- (void) addComment: (id) sender;
- (void) deleteComment: (id) sender;

// ****************************
// NSTableView delegate methods
// ****************************

- (void)tableView:(NSTableView *)aTableView 
  willDisplayCell:(id)aCell
   forTableColumn:(NSTableColumn *)aTableColumn 
              row:(int)rowIndex;

- (BOOL)      tableView: (NSTableView *)aTableView
  shouldEditTableColumn: (NSTableColumn *) aTableColumn 
                    row: (int) index;

@end

#endif // _EDITORWINDOWCONTROLLER_H_

