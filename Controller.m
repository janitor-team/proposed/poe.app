/*
    Controller.m - Application controller for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "Controller.h"

@implementation Controller

- (void) awakeFromNib
{
  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
  NSArray *temp;
  NSMenu *men;

  if([def arrayForKey: @"defaultTags"] == nil)
    {
      temp = [NSArray arrayWithObjects: @"TITLE",@"ARTIST",@"ALBUM",nil];
      [def setObject: temp forKey: @"defaultTags"];
    }
  
  // localize the info menu
  
  men = [[[NSApp mainMenu] itemWithTitle: @"Info"] submenu];
  [men setTitle: _(@"Info")];
  
  [[men itemWithTitle: @"Info Panel..."] setTitle: _(@"Info Panel...")];
  [[men itemWithTitle: @"Preferences..."] setTitle: _(@"Preferences...")];
  [[men itemWithTitle: @"Help..."] setTitle: _(@"Help...")];

  // localize the Document menu
  
  men = [[[NSApp mainMenu] itemWithTitle: @"Document"] submenu];
  [men setTitle: _(@"Document")];
  
  [[men itemWithTitle: @"Open..."] setTitle: _(@"Open...")];
  [[men itemWithTitle: @"Save..."] setTitle: _(@"Save...")];
  [[men itemWithTitle: @"Close"] setTitle: _(@"Close")];

  // localize the Edit menu
  
  men = [[[NSApp mainMenu] itemWithTitle: @"Edit"] submenu];
  [men setTitle: _(@"Edit")];
  
  [[men itemWithTitle: @"Cut"] setTitle: _(@"Cut")];
  [[men itemWithTitle: @"Copy"] setTitle: _(@"Copy")];
  [[men itemWithTitle: @"Paste"] setTitle: _(@"Paste")];
  [[men itemWithTitle: @"Add Comment..."] setTitle: _(@"Add Comment...")];
  [[men itemWithTitle: @"Delete Comment"] setTitle: _(@"Delete Comment")];
  
  // localize the Windows menu
  
  men = [[[NSApp mainMenu] itemWithTitle: @"Windows"] submenu];
  [men setTitle: _(@"Windows")];
  
  [[men itemWithTitle: @"Arrange In Front"] setTitle: _(@"Arrange In Front")];
  [[men itemWithTitle: @"Miniaturize Window"] setTitle: _(@"Miniaturize Window")];
  [[men itemWithTitle: @"Close Window"] setTitle: _(@"Close Window")];

  // localize the main menu
  
  men = [NSApp mainMenu];
  
  [[men itemWithTitle: @"Info"] setTitle: _(@"Info")];
  [[men itemWithTitle: @"Document"] setTitle: _(@"Document")];
  [[men itemWithTitle: @"Edit"] setTitle: _(@"Edit")];
  [[men itemWithTitle: @"Windows"] setTitle: _(@"Windows")];
  [[men itemWithTitle: @"Services"] setTitle: _(@"Services")];
  [[men itemWithTitle: @"Hide"] setTitle: _(@"Hide")];
  [[men itemWithTitle: @"Quit"] setTitle: _(@"Quit")];
  
}

- (void) showPrefs: (id) sender
{
  [[PreferencesWindowController singleInstance] showWindow: self];
}

@end
