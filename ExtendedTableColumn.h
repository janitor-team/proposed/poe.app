/* 
   ExtendedTableColumn.h

   Copyright (c) 2001 Pierre-Yves Rivaille

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; see the file COPYING.LIB.
   If not, write to the Free Software Foundation,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#ifndef _GNUMail_H_ExtendedTableColumn
#define _GNUMail_H_ExtendedTableColumn

#include <AppKit/AppKit.h>

@interface ExtendedTableColumn: NSTableColumn
{
  BOOL _setTag;
  BOOL _setState;
  BOOL _useMouse;
}
- (BOOL) shouldUseTag;
- (BOOL) shouldUseAndSetState;
- (BOOL) shouldUseMouse;
- (void) setShouldUseTag: (BOOL) aBool;
- (void) setShouldUseAndSetState: (BOOL) aBool;
- (void) setShouldUseMouse: (BOOL) aBool;
@end

@interface NSTableColumn (ExtendedExtensions)
- (BOOL) shouldUseTag;
- (BOOL) shouldUseAndSetState;
- (BOOL) shouldUseMouse;
@end

#endif // _GNUMail_H_ExtendedTableColumn
