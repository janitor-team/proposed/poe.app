/* 
   ExtendedTableColumn.m

   Copyright (c) 2001 Pierre-Yves Rivaille

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; see the file COPYING.LIB.
   If not, write to the Free Software Foundation,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#include "ExtendedTableColumn.h"

@implementation ExtendedTableColumn

- (id)initWithIdentifier: (id)anObject
{
  self = [super initWithIdentifier: anObject];
  _setTag = NO;
  _setState = NO;
  _useMouse = NO;

  return self;
}


- (BOOL) shouldUseTag
{
  return _setTag;
}


- (BOOL) shouldUseAndSetState
{
  return _setState;
}


- (BOOL) shouldUseMouse
{
  return _useMouse;
}


- (void) setShouldUseTag: (BOOL) aBool
{
  _setTag = aBool;
}


- (void) setShouldUseAndSetState: (BOOL) aBool
{
  _setState = aBool;
}


- (void) setShouldUseMouse: (BOOL) aBool
{
  _useMouse = aBool;
}


@end


@implementation NSTableColumn (ExtendedExtensions)


- (BOOL) shouldUseTag
{
  return NO;
}


- (BOOL) shouldUseAndSetState
{
  return NO;
}


- (BOOL) shouldUseMouse
{
  return NO;
}


@end

