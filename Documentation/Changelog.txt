Thu May  5 00:52:15 UTC 2005
	Added and/or Completed Bulgarian, Japanese, Thai, and German Localizations
	Fixed a memory leak
	automatic resizing of the title column in the comment view
    Moved menu localization to Localizable.strings. Now no gorms need be edited.
	Add Comment panel bug fix

Wed Mar 10 10:41:59 UTC 2004
	Added Italian localization from Katjia Mirri
	fixed Windows, and Service menus in Thai and English lproj's
	PreferencesWindowController.m : set and save window frame using the defaults
	
Fri May  2 13:07:15 EDT 2003
	Resources/English.lproj/Preferences.gorm
	removed the resize bar from the Preferences window.
	Documentation/Changelog.txt; new file.
