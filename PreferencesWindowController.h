/*
    PreferencesWindowController.h - Preferences Window  controller 
    header for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#ifndef _PREFERENCESWINDOWCONTROLLER_H_
#define _PREFERENCESWINDOWCONTROLLER_H_

#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "SwitchTableView.h"
#include "ExtendedTableColumn.h"
#include "Util.h"
#include "NSMutableArray+goodies.h"

@interface PreferencesWindowController : NSWindowController
{
  IBOutlet NSBox *box;
  id tagsTable;
  id tagsTableSV;
  id tagDesc;
  id descView;

  id genreView;
  id addButton;
  id deleteButton;
  id genreBrowser;
  id genreText;

  id label;

  NSMutableArray *_commentTags;
  NSMutableArray *_genres;

  int gvFlag;
}

+ (id) singleInstance;

- (void) addGenre: (id)sender;
- (void) removeGenre: (id)sender;

// Private Methods
//*****************

- (void) _setupData;
- (void) _setupTable;
- (void) _setupGenreView;

// NSTableView data source protocol methods
//******************************************

- (int) numberOfRowsInTableView: (NSTableView *)aTableView;

- (id)            tableView: (NSTableView *)aTableView
  objectValueForTableColumn: (NSTableColumn *)aTableColumn
                        row: (int)rowIndex;

// SwitchTableView data source protocol methods
//**********************************************

- (int)    tableView: (NSTableView *)aTableView
 stateForTableColumn: (NSTableColumn *)aTableColumn
                 row: (int)rowIndex;

- (void)   tableView: (NSTableView *)aTableView
            setState: (int)aState
      forTableColumn: (NSTableColumn *)aTableColumn
                 row: (int)rowIndex;

// NSTableView delegate methods
//******************************

- (void)tableViewSelectionIsChanging:(NSNotification *)aNotification;

// NSBrowser delegate methods
//****************************

- (int)browser:(NSBrowser *)sender numberOfRowsInColumn:(int)column;

- (void)browser:(NSBrowser *)sender willDisplayCell:(id)cell
  atRow:(int)row column:(int)column;


@end

#endif // _PREFERENCESWINDOWCONTROLLER_H_

