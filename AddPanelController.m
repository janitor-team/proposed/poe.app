/*
    AddPanelController.m - Add comment panel controller for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "AddPanelController.h"

@implementation AddPanelController

- (void) dealloc
{
	RELEASE(_selection);
	RELEASE(_tags);

	[super dealloc];
}

- (id) initWithPosition: (NSPoint) origin
{
  self = [super initWithWindowNibName: @"AddPanel" owner: self];
  
  [[self window] setFrameOrigin: origin];

  return self;
}

- (void) awakeFromNib
{
 	[[self window] setTitle: _(@"Add Comment")];
	[label setStringValue: _(@"Choose a comment to add:")];
	[okButton setStringValue: _(@"OK")];
	[cancelButton setStringValue: _(@"Cancel")];

	_tags = [[NSMutableArray alloc] initWithArray: 
	[[Util singleInstance] tagsTitle]]; 
	[tagBrowser loadColumnZero];
	_selection = @"none";

 	[[self window] setDefaultButtonCell: [okButton cell]];
}

- (void) removeTag: (NSString *)tag
{
  if([_tags containsObject: tag])
    {
      [_tags removeObjectAtIndex: [_tags indexOfObject: tag]];
      [tagBrowser loadColumnZero];
    }
}

- (void) okPressed: (id) sender;
{
	if( [tagBrowser selectedRowInColumn: 0] > -1 ) 
		_selection = [[tagBrowser selectedCell] stringValue];
	[NSApp stopModal];
	[[self window] close];
}

- (void) cancelPressed: (id) sender
{
  [NSApp stopModal];
  [[self window] close];
}

- (NSString *) selection;
{
  return _selection;
}

// NSBrowser delegate methods
//****************************

- (int)browser:(NSBrowser *)sender numberOfRowsInColumn:(int)column
{
  if(sender == tagBrowser)
    {
      return [_tags count];
    }
  else
    {
      return 0;
    }
}

- (void)browser:(NSBrowser *)sender willDisplayCell:(id)cell
  atRow:(int)row column:(int)column
{
  if(sender == tagBrowser)
    {
      [cell setLeaf: YES];
      [cell setStringValue: [_tags objectAtIndex: row]];
    }
}

@end

