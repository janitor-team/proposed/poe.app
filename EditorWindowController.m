/*
    EditorWindowController.m - Editor window controller for Poe.app
    Copyright (C) 2003,2004,2005 Rob Burns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02111, USA.
*/

#include "EditorWindowController.h"

@implementation EditorWindowController

- (void) dealloc
{
  RELEASE(_oggFile);
  [super dealloc];
}

- (id) initWithEditor: (OGGEditor *) anOggFile
{
  NSString *time;

  ASSIGN(_oggFile,anOggFile);
  
  self = [super initWithWindowNibName: @"Editor" owner: self];
 
  [[self window] setTitle: [[_oggFile filename] lastPathComponent]];
  [commentView setDataSource: _oggFile];
  [commentView setAutoresizesAllColumnsToFit: NO];
  [commentView sizeLastColumnToFit];
  [commentView setCornerView: nil];
  [commentView setHeaderView: nil];

#ifdef GNUstep
  [cardsView setRowHeight: [[NSFont systemFontOfSize:
      [NSFont systemFontSize]] boundingRectForFont].size.height+5];
#endif
      
  [lengthT setStringValue: _(@"Length:")];
  [channelsT setStringValue: _(@"Channels:")];
  [sampleT setStringValue: _(@"Sample Rate:")];
  [bitrateT setStringValue: _(@"Nominal Bitrate:")];
  [hbitrateT setStringValue: _(@"High Bitrate:")];
  [lbitrateT setStringValue: _(@"Low Bitrate:")];

  int raw_seconds = [[_oggFile seconds] intValue];
  int minutes = raw_seconds / 60;
  int seconds = raw_seconds - (minutes * 60);
  if(seconds < 10)
    time = [NSString stringWithFormat: @"%i:0%i",minutes,seconds];
  else
    time = [NSString stringWithFormat: @"%i:%i",minutes,seconds];

  [length setStringValue: time];
  [channels setStringValue: 
    [[_oggFile channels] descriptionWithLocale: nil]];
  [nominalOfBitrate setStringValue:
    [[_oggFile nominalOfBitrate] descriptionWithLocale: nil]];
  [upperOfBitrate setStringValue:
    [[_oggFile upperOfBitrate] descriptionWithLocale: nil]];
  [lowerOfBitrate setStringValue:
    [[_oggFile lowerOfBitrate] descriptionWithLocale: nil]];
  [rate setStringValue: [[_oggFile rate] descriptionWithLocale: nil]];

  return self;
}

- (void) addComment: (id) sender
{
  AddPanelController *panel;
  NSMutableDictionary *temp;
  NSArray *cm;
  NSPoint origin;
  int x, y;
  int i;

  x = [[self window] frame].origin.x + ([[self window] frame].size.width/2);
  y = [[self window] frame].origin.y + ([[self window] frame].size.height/2); 
  origin = NSMakePoint(x-(217/2),y-(183/2));

  cm = [_oggFile comments];
  panel = [[AddPanelController alloc] initWithPosition: origin];

  for(i=0;i<[[_oggFile comments] count];i++)
    {
      if(![[[cm objectAtIndex: i] objectForKey: @"tag"] isEqualToString: @"GENRE"] &&
        ![[[cm objectAtIndex: i] objectForKey: @"tag"] isEqualToString: @"ARTIST"] &&
        ![[[cm objectAtIndex: i] objectForKey: @"tag"] isEqualToString: @"PERFORMER"])
        {
          [panel removeTag: [[cm objectAtIndex: i] objectForKey: @"title"]];
        }
    }

  [NSApp runModalForWindow: [panel window]];

  if(![[panel selection] isEqualToString: @"none"])
    {
      i = [[Util singleInstance] indexOfTitle: [panel selection]];
     
      if(1 != -1) 
        {
          temp = [NSMutableDictionary new];
          [temp setObject: [[[Util singleInstance] tags] objectAtIndex: i]
            forKey: @"tag"];
          [temp setObject: [[[Util singleInstance] tagsTitle] objectAtIndex: i]
            forKey: @"title"];
          [temp setObject: @"" forKey: @"value"];

          [_oggFile addComment: temp];

          [commentView reloadData];
          [[self document] updateChangeCount: NSChangeDone];
        }  
    }
  RELEASE(panel);
}

- (void) deleteComment: (id) sender
{
  if([commentView selectedRow])
    {
      [_oggFile deleteCommentAtIndex: [commentView selectedRow]];
      [commentView reloadData];
      [[self document] updateChangeCount: NSChangeDone];
    }
}

// ****************************
// NSTableView delegate methods
// ****************************

- (void)tableView:(NSTableView *)aTableView
  willDisplayCell:(id)aCell
   forTableColumn:(NSTableColumn *)aTableColumn
              row:(int)rowIndex
{
//  NSComboBoxCell *tCell;
  float width;

  if([[aTableColumn identifier] isEqualToString: @"title"])
    {
      [aCell setEditable: NO];
      [aCell setSelectable: NO];
      [aCell setFont: [NSFont boldSystemFontOfSize: 12]];
      width = [[aCell attributedStringValue] size].width;
      if(width > [aTableColumn width]-10)
		{
    			[aTableColumn setWidth: width+10];
    			[aTableView sizeLastColumnToFit];
					[aTableView display];
		}
    }

//  if([[aTableColumn identifier] isEqualToString: @"value"] &&
//    [[[[_oggFile comments] objectAtIndex: rowIndex] objectForKey: @"tag"] 
//      isEqualToString: @"GENRE"])
//    {
//      tCell = [[NSComboBoxCell alloc] init];
//      [tCell setUsesDataSource: NO];
//      [tCell addItemsWithObjectValues: [[Util singleInstance] genres]];
//      aCell = tCell;
//    }
}

- (BOOL)      tableView: (NSTableView *)aTableView
  shouldEditTableColumn: (NSTableColumn *) aTableColumn 
                    row: (int) index
{
  [[self document] updateChangeCount: NSChangeDone];
  return YES;
}

 
@end
